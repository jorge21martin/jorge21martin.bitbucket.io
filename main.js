//Constants
WIDTH     = 1000;
HEIGHT    = 500;
BG_COLOR  = "green";
RUNNING   = true;
FPS       = 30;
BG        = new Image();
BG.src    = "sky1.png";

var canvas = document.getElementById("canvas");
canvas.style.width  = WIDTH + "px";
canvas.style.height = HEIGHT + "px";
ctx = canvas.getContext("2d");
ctx.fillStyle = "black";

projectile = new Projectile(25, 75, 5, 5, 0, 0);

function update(dt) {
	time +=(dt);	
	projectile.accelerate(time);
}
var x_el = document.getElementById("x");
var y_el = document.getElementById("y");
var vx_el = document.getElementById("vx");
var vy_el = document.getElementById("vy");
var t_el = document.getElementById("t");
time = 0;

last_y = 0;
function html(dt) {
	x_el.innerHTML = "X: " + Math.round(projectile.x);
	y_el.innerHTML = "Y: " + Math.round(projectile.y);
	vx_el.innerHTML = "VX: " + Math.round(projectile.velocity.x);
	vy_el.innerHTML = "VY: " + Math.round(projectile.velocity.y);
	if ((last_y - projectile.y) != 0) {	
		t_el.innerHTML = "T: " + (time / 1000).toFixed(2);
	}
	last_y = projectile.y;
}

path = [];
function draw() {
	//ctx.fillStyle = "white";
	//ctx.fillRect(0, 0, canvas.width, canvas.height);
	ctx.drawImage(BG, 0, 0, canvas.width, canvas.height);
	ctx.fillStyle = "gray";
	ctx.fillRect(12.5, 80, 30, (160-85));
	ctx.fillStyle = "blue";
	projectile.draw(ctx);
	ctx.fillStyle = "red";
	path.push([projectile.x, projectile.y]);
	for (var i=0;i<path.length;i++) {	
		ctx.fillRect((path[i][0] + 2), (path[i][1] + 2.5), 1, 1)
	}
}

//Start Program
lastFrameTimeMs = 0;
function loop() {
	if (running) {
		var dt = 1000 / FPS;
		update(dt);
		draw();
		html(dt);
	} else {
		draw();
		html(dt);
	}
}

var running = false;
var button = document.getElementById("start");
button.addEventListener("click", function() {
	if (!running) {
		var angle = document.getElementById("angle_input").value;
		var rad = (angle * Math.PI) / 180;
		var velocity = document.getElementById("v_input").value;
		projectile.velocity.x = velocity * Math.cos(rad);
		projectile.velocity.yi = -1 * (velocity * Math.sin(rad));
		running = true;	
	}
});
window.setInterval(loop, (1000 / FPS));
