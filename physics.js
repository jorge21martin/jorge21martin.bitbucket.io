//Constants
GRAVITY = 9.8;
SCALE = 1;

class Projectile {
	constructor(x, y, width, height, vx, vy) {
		this.x = x;
		this.y = y;
		this.xi = x;
		this.yi = y;
		this.last_y = y;	
		this.width = width;
		this.height = height;	
		this.velocity = new Object();
		this.velocity.x = vx;
		this.velocity.y = vy;
		this.velocity.yi = vy;
		this.acceleration = new Object();
		this.acceleration.x = 0;
		this.acceleration.y = GRAVITY;	
	}

	accelerate(time) {
		time = time / 1000;	
		this.acceleration = new Object();
		this.acceleration.x = 0;
		this.acceleration.y = GRAVITY;	

		var delta_x = this.velocity.x * time;
		var delta_y = ((this.velocity.yi * time) + (4.9 * Math.pow(time, 2)));
		if ((delta_y + parseInt(this.yi)) < ((150-this.height) / SCALE)) {
			this.x = this.xi + delta_x;
			var negative = false;
			this.y = parseInt(this.yi) + delta_y;
			if (this.last_y < this.y) {
				negative = true;
			}
			this.last_y = this.y;

			this.velocity.y = (GRAVITY * time) + this.velocity.yi;
		}
	}

	draw(ctx) {
		ctx.fillRect(SCALE * this.x, SCALE * this.y, this.width, this.height);	
	}
}
